import axios from "axios";

const url = "http://jsonplaceholder.typicode.com/todos/1";

axios.get(url).then(response => {
  // console.log(response.data);

  const todo = response.data;

  const ID = todo.ID;
  const title = todo.Title;
  const finished = todo.finished;

  console.log(`
  The TODO with ID:${ID}
  Has a title of: ${title}
  Is it finished? ${finished}
  `)
})